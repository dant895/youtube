#language: es
#author: Daniel Torres Bedoya
Característica: En esta suite de pruebas
  se realizarán los escenarios correspondientes
  a los flujos diseñados la realización de la prueba
  de Devco

  Antecedentes: 
    Dado que Daniel quiere ingresar a Youtube

  @Caso1
  Escenario: Reproducir un video
    Cuando Daniel reproduce el video: "Kany García - Bailemos un Blues (Official Video)"

  @Caso2
  Escenario: Iniciar sesion
    Cuando Daniel inicia sesión con credenciales
      | Usuario | Contrasenia |
      | usuario | password    |
    Entonces debe visualizar la página de inicio con sus recomedaciones de videos

  @Caso3
  Escenario: Suscribirse a un canal
    Cuando: Daniel inicia sesión con sus credenciales
      | Usuario | Contrasenia |
      | usuario | password    |

    Y busca un canal, Daniel se suscribe

  @Caso4
  Escenario: Añadir comentarios a videos
    Cuando Daniel inicia sesión con sus credenciales
      | Usuario | Contrasenia |
      | usuario | password    |
    Y Daniel reproduce el video: "Kany García - Bailemos un Blues (Official Video)"
    Y adiciona el comentario "" al video

  @Caso5
  Escenario: Busca las tendencias en videos
    Cuando Daniel busca las tendencias de videos en la categoria "Música"

  @Caso6
  Escenario: Subir un video al canal
    Cuando Daniel inicia sesión con sus credenciales
    Y Carga el video "" al canal
    Entonces Daniel debe ver el video en la lista de videos cargados