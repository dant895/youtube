package stepsdefinitions;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import questions.ReproduccionVideo;
import tasks.AbrirElNavegador;
import tasks.Buscar;
import tasks.Reproducir;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static utils.Constantes.*;

public class EscenariosStepDefinition {
	@Managed(driver = NAVEGADOR)
	private WebDriver suNavegador;
	public static final Actor Daniel = Actor.named("Daniel");

	@Before
	public void prepararEscenario() {
		OnStage.setTheStage(new OnlineCast());
	}

	@Before
	public void inicializacion() {
		Daniel.can(BrowseTheWeb.with(suNavegador));
	}

	@Cuando("que Daniel quiere ingresar a Youtube")
	public void queJuanSeQuiereLoguearseEnLaAplicacionDeSufi() {
		Daniel.wasAbleTo(AbrirElNavegador.enYouTube());
	}
	
	@Cuando("^Daniel reproduce el video: \"([^\"]*)\"$")
	public void danielBuscaLaCanciónYLoReproduce(String video) throws Exception {
		Daniel.wasAbleTo(Buscar.unaCancion(video));
		Daniel.wasAbleTo(Reproducir.unaCancion(video));
		Daniel.should(seeThat(ReproduccionVideo.esCorrecta(video)));
	}
}
