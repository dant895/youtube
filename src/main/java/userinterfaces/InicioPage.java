package userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.youtube.com")
public class InicioPage extends PageObject{

	public static final Target BUSCADOR = Target.the("Barra de búsqueda")
			.locatedBy("//input[@id='search']");
	public static final Target BTB_BUSQUEDA = Target.the("Botón de búsqueda")
			.locatedBy("//button[@id='search-icon-legacy']");
}