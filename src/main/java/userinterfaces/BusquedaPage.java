package userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class BusquedaPage extends PageObject {

	public static final Target ENLACE_VIDEO = Target.the("Enlace del video")
			.locatedBy("//*/div/h3/a[contains(text(),'{0}')]");

	public static final Target TITULO_VIDEO = Target.the("Título del video")
			.locatedBy("//*[@id=\"container\"]/h1/yt-formatted-string[contains(text(),'{0}')]");

}