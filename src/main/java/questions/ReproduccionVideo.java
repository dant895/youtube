package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import static userinterfaces.BusquedaPage.*;

public class ReproduccionVideo implements Question<Boolean> {
	private String titulo;

	public ReproduccionVideo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public Boolean answeredBy(Actor actor) {
		boolean validacion = true;

		String tituloPage = Text.of(TITULO_VIDEO.of(titulo)).viewedBy(actor).asString();
		if (!titulo.equals(tituloPage)) {
			validacion = false;
		}
		return validacion;
	}

	public static ReproduccionVideo esCorrecta(String titulo) {
		return new ReproduccionVideo(titulo);
	}
}
