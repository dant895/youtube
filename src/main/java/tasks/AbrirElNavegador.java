package tasks;


import userinterfaces.InicioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirElNavegador implements Task {
	
	private InicioPage homePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(homePage));
	}

	public static AbrirElNavegador enYouTube() {
		
		return Tasks.instrumented(AbrirElNavegador.class);
	}

}
