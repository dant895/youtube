package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import static userinterfaces.BusquedaPage.*;

public class Reproducir implements Task {
	private String cancion;

	public Reproducir(String cancion) {
		this.cancion = cancion;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(ENLACE_VIDEO.of(cancion)));
	}

	public static Reproducir unaCancion(String cancion) {
		return Tasks.instrumented(Reproducir.class, cancion);
	}
}
