package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import static userinterfaces.InicioPage.*;

public class Buscar implements Task {
	private String cancion;

	public Buscar(String cancion) {
		this.cancion = cancion;

	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(cancion).into(BUSCADOR), Click.on(BTB_BUSQUEDA));
	}

	public static Buscar unaCancion(String cancion) {

		return Tasks.instrumented(Buscar.class, cancion);
	}
}
