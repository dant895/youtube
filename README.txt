Se tienen las siguientes consideraciones:
1. El presente proyecto contempla el flujo automatizado del flujo: reproducción de un video en Youtube.
2. El usuario tiene la opción de modificar el video que desea reproducir modificando el nombre del video en el Caso1 del archivo: "escenarios.feature".
3. En los steps deficions se realizaron los pasos correspondientes para completar el flujo, como lo son: buscar video, reproducir video y 
una verificación para corroborar que se reproduzca el video correcto.
4. Se plasmaron cinco escenarios de flujos dentro de la aplicación los cuales no tienen pasos o tareas desarrolladas dentro del proyecto.